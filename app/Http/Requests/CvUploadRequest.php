<?php

namespace App\Http\Requests;

use App\Rules\AbilityRules;
use App\Rules\FileRules;
use Illuminate\Foundation\Http\FormRequest;

class CvUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => ['required' , 'max:255' , 'alpha'],
            'lastName' => ['required' , 'max:255' , 'alpha'],
            'cvFile' => ['file', 'required' , new FileRules()],
            'ability' => [new AbilityRules()],
            'email' => ['nullable' , 'email:rfc'],
            'phone' => ['nullable' , 'digits:11'],
        ];
    }
}
