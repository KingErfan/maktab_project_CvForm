<?php

namespace App\Http\Controllers;

use App\Http\Requests\CvUploadRequest;
use Illuminate\Http\Request;

class CvFormController extends Controller
{
    public function index()
    {
        return view('CvFormView');
    }

    public function cvListShow()
    {
        //get list from DB
        $data = \DB::table('cvUser')->select('name' , 'email' , 'cvFileName as cvFile' , 'ability')->get();

        // exploding abilities to array
        foreach ($data as $index => $value)
        {
            $abilities = explode('_' , $value->ability);
            $data[$index]->ability = $abilities;
        }

        // return view file
        return view('CvFormList' , ['records' => $data]);
    }

    public function cvFileDownload($fileName)
    {
        return \Storage::download('/files/' . $fileName);
    }

    public function handleCv(CvUploadRequest $request)
    {
        $validated = $request->validated();

        $validated['cvFileName'] = $this->storeToStorage($validated);

        $this->storeToDb($validated);

        return redirect('/cvform/list');
    }

    private function storeToStorage($data)
    {
        $fileName = $data['firstName'] . $data['lastName'] . "_" . request()->file('cvFile')->getClientOriginalName();

         request()->file('cvFile')->storeAs('files' , $fileName);
         return $fileName;
    }

    private function storeToDb($data)
    {
        \DB::table('cvUser')->insert(
            [
                'name' => $data['firstName'] . ' ' . $data['lastName'],
                'email' => (isset($data['email'])) ? $data['email'] : null,
                'phone' => (isset($data['phone'])) ? $data['phone'] : null ,
                'ability' => implode(' _ ' , $data['ability']),
                'cvFileName' => $data['cvFileName'],
            ]
        );
    }

}
