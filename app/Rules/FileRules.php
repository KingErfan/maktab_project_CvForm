<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FileRules implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // return false for extension
        if (!$this->extensionCheck())
            return false;

        //return false for size
        if (!$this->checkSize())
            return false;

        //return true for all validation goes true
        return true;
    }

    /**
     * check file size to be under 5 mb
     *
     * @return bool
     */
    private function checkSize()
    {
        return request()->file('cvFile')->getSize() < 5000000;
    }

    /**
     * check file extension to be 'pdf'
     *
     * @return bool
     */
    private function extensionCheck()
    {
        return request()->file('cvFile')->extension() === 'pdf';
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The CvFile should be in pdf format and under 5mb.';
    }
}
