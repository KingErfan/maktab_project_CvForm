<?php

namespace App\Rules;

use http\Exception\UnexpectedValueException;
use Illuminate\Contracts\Validation\Rule;

class AbilityRules implements Rule
{

    private $abilities = ['ability 1' , 'ability 2' , 'ability 3' , 'ability 4' , 'ability 5'];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // length check
        if (!$this->checkLength($value))
            return false;
        //existence check
        if (!$this->abilityExists($value))
            return false;
        // return true if all validation goes true
        return true;
    }

    private function abilityExists($array)
    {
        foreach ($array as $item)
        {
            // return false if an item does not exists in array
            if (!in_array($item , $this->abilities))
                return false;
        }
        // returns true if all items exist in ability array
        return true;
    }

    private function checkLength($array)
    {
        return (count($array) >= 3) And ( count($array) <= count($this->abilities) );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You have to choose at least 3 abilities.';
    }
}
