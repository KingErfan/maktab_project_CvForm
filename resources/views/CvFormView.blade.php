@extends('layouts.layout')

@section('title' , 'CvForm')

@section('bar_title' , 'MejGod')

@section('content')
    <div class="container mt-3">
        <form action="/cvform/upload" method="post" enctype="multipart/form-data">
            @csrf
            {{-- Name --}}
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="firstName">First Name</label>
                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Mohammad Erfan">
                </div>
                @error('firstName')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group col-md-6">
                    <label for="lastName">Last Name</label>
                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Jabbari">
                </div>
                @error('lastName')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            {{-- File --}}
            <div class="form-group">
                <label for="cvFile">Upload your CV file here :</label>
                <input type="file" class="form-control-file form-control-lg" name="cvFile" id="cvFile" placeholder="Cv File">
            </div>
            @error('cvFile')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- Email Address --}}
            <div class="form-group">
                <label for="email" class="col-12">Email Address :</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="example@example.com">
            </div>
            @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- Ability --}}
            <div class="form-group">
                <label>Abilities :</label>
                <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="ability-1" name="ability[]" value="ability 1">
                    <label class="form-check-label" for="ability-1">Ability 1</label>
                </div>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="ability-2" value="ability 2" name="ability[]">
                    <label class="form-check-label" for="ability-2">Ability 2</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="ability-3" value="ability 3" name="ability[]">
                    <label class="form-check-label" for="ability-3">Ability 3</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="ability-4" value="ability 4" name="ability[]">
                    <label class="form-check-label" for="ability-4">Ability 4</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="ability-5" value="ability 5" name="ability[]">
                    <label class="form-check-label" for="ability-5">Ability 5</label>
                </div>
            </div>
            @error('ability')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- Phone Number --}}
            <div class="form-group">
                <label for="phone">Phone Number</label>
                <input type="tel" class="form-control" name="phone" id="phone" placeholder="091xxxxxxxx">
            </div>
            @error('phone')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- Submit --}}
            <button type="submit" class="btn btn-primary">Sign up</button>
        </form>
    </div>
@endsection
