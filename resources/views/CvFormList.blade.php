@extends('layouts.app')

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr>
                <td>Name</td>
                <td>email</td>
                <td>Cv File</td>
                <td colspan="4">Abilities</td>
            </tr>
        </thead>
        <tbody>
            @foreach($records as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>
                        <a href="/cvfile/{{$item->cvFile}}">{{$item->cvFile}}</a>
                    </td>
                    @foreach($item->ability as $value)
                        <td>{{$value}}</td>
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
