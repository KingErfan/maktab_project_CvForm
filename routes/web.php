<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CvFormController as CvFormController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// load cfvForm view
Route::get('/cvform' , [CvFormController::class , 'index']);

Route::post('/cvform/upload' , [CvFormController::class , 'handleCv'] ) ;

Route::get('/cvform/list' , [CvFormController::class , 'cvListShow'] ) ;

Route::get('cvfile/{fileName}' , [CvFormController::class , 'cvFileDownload']);

